terraform {
  backend "s3" {
    bucket         = "recipe-app-api-devops-tfstate-cuong"
    key            = "recipe-app-tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 3.25.0"
}

locals {
  prefix = "${var.prefix}--${terraform.workspace}"
  common_tag = {
    Environment = "dev"
    Project     = "${var.project}"
    Owner       = "${var.contact}"
    ManagedBy   = "Terraform"
  }
}